import React, { Component } from "react";

export default class Cart extends Component {
  renderTBody = () => {
    return this.props.cart.map((item) => {
      return (
        <tr>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>
            <img
              style={{
                width: "50px",
              }}
              src={item.image}
              alt=""
            />
          </td>
          <td>{item.price * item.number}$</td>
          <td>
            <button className="btn btn-danger">-</button>
            <span className="mx-2">{item.number}</span>
            <button className="btn btn-success">+</button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <table className="table">
        <thead>
          <td>Id</td>
          <td>Name</td>
          <td>Img</td>
          <td>Price</td>
          <td>Quantity(số lương)</td>
        </thead>
        <tbody>{this.renderTBody()}</tbody>
      </table>
    );
  }
}
