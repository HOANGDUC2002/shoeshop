import React, { Component } from "react";

export default class DetailShoe extends Component {
  render() {
    let { id, image, price, description, name } = this.props.detail;
    return (
      <div className="row mt-5 alert-secondary p-5 text-left">
        <img className="col-3" src={image} alt="" />
        <div className="col-9">
          <p>ID:{id}</p>
          <p>Name:{name}</p>
          <p>Desc:{description}</p>
          <p>Price:{price}$</p>
          <p>Quantity(số lượng)</p>
        </div>
      </div>
    );
  }
}
