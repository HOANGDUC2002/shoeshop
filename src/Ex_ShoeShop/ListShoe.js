import React, { Component } from "react";
import ItemShoe from "./ItemShoe";
export default class ListShoe extends Component {
  renderListShoe = () => {
    return this.props.shoeArr.map((item) => {
      return (
        <ItemShoe
          handleChangeShoe={this.props.handleChangeShoe}
          data={item}
          handleAddToCart={this.props.handleAddToCart}
        />
      );
    });
  };

  render() {
    return <div className="row">{this.renderListShoe()}</div>;
  }
}
