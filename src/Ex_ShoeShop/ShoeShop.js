import React, { Component } from "react";
import Cart from "./Cart";
import { dataShoe } from "./dataShoe";
import DetailShoe from "./DetailShoe";
import ItemShoe from "./ItemShoe";
import ListShoe from "./ListShoe";

export default class ShoeShop extends Component {
  state = {
    shoeArr: dataShoe,
    detail: dataShoe[0],
    cart: [],
  };

  handleChangeShoe = (shoe) => {
    this.setState({
      detail: shoe,
    });
  };
  handleAddToCart = (shoe) => {
    let cloneCart = [...this.state.cart];
    let index = this.state.cart.findIndex((item) => {
      return item.id == shoe.id;
    });
    if (index == -1) {
      let cartItem = { ...shoe, number: 1 };
      cloneCart.push(cartItem);
    } else {
      cloneCart[index].number++;
    }
    this.setState({ cart: cloneCart });
  };
  render() {
    return (
      <div className="container py-5">
        <Cart cart={this.state.cart} />
        <ListShoe
          handleChangeShoe={this.handleChangeShoe}
          handleAddToCart={this.handleAddToCart}
          shoeArr={this.state.shoeArr}
        />
        <DetailShoe detail={this.state.detail} />
      </div>
    );
  }
}
